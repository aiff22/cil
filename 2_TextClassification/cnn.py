# Training CNN on the obtained vector representation
# of the words (and sentences)

#Ideas: Reduced Stride for a more sliding window

from gensim.models import word2vec
import numpy as np
import tensorflow as tf
import os
import time
import sys



# Defining encapsulated tf functions for CNN
def weight_variable(shape, stddev, pName):
    initial = tf.truncated_normal(shape, stddev=stddev) # trunctated normal -> Random values not more than 2 times stddeviation away
    return tf.Variable(initial, name=pName)
  
  
def bias_variable(shape, pName):
    '''

    '''
    initial = tf.constant(0.01, shape=shape) #Variable is constant in the beginning
    return tf.Variable(initial, name=pName)

def conv2d(x, W):
    '''
    "Valid":   Only full convolutions:     out_height = ceil(float(in_height - filter_height + 1) / float(strides[1]))
    "Same" :   Fill up with zeros:         out_height = ceil(float(in_height) / float(strides[1]))
    '''
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='VALID') 

def max_pool_layer(x):
    return tf.nn.max_pool(x, ksize=[1, 1, max_sentence_length - filter_size_1 + 1, 1], 
                          strides=[1, 1, max_sentence_length - filter_size_1 + 1, 1], padding='VALID')


def avg_pool_layer(x):
    return tf.nn.avg_pool(x, ksize=[1, 1, max_sentence_length - filter_size_1 + 1, 1], 
                          strides=[1, 1, max_sentence_length - filter_size_1 + 1, 1], padding='VALID')


OverallamountOfUnknownWords = 0
def sentence_to_vector(s):
    ''' Transforming sentences to matrixes 
    Input:
        s:      A single sentence
    '''
    global OverallamountOfUnknownWords
    try:
      if MODEL == "Google":
        sv = google_model[filter(lambda x: x in google_model.vocab, s.split())] # sv has dimension (16,100)
        sv = np.stack(sv)
      elif MODEL == "Own":
        sv = own_model[filter(lambda x: x in own_model.vocab, s.split())] # sv has dimension (16,100)
        sv = np.stack(sv)
      else:
        sv = [stanford_dict_model[cur] for cur in s.split() if cur in stanford_dict_model]
        sv = np.stack(sv)
    except Exception:
        OverallamountOfUnknownWords += 1
        print("Unknown Word! (" + str(OverallamountOfUnknownWords) + ")")
        sv = np.zeros((0, embeddings_size))

    # All obtained sentence matrixes should be of the same length,
    # and therefore they are either cut or padded with additional columns
    
    if sv.shape[0] > max_sentence_length:
        sv = sv[(sv.shape[0] - max_sentence_length):]
    else:
        sv = np.vstack((sv, np.zeros((max_sentence_length - sv.shape[0], embeddings_size))))

    sv = np.reshape(np.transpose(sv), [1, embeddings_size * max_sentence_length])
    
    return sv





#########################################################
######################### MAIN ##########################
#########################################################
if __name__ == "__main__":
    directory = os.path.dirname(os.path.realpath(__file__))
    MODEL = sys.argv[1]

    # Choose subfolder for results
    SUBFOLDER = "/"+str(int(time.time()))
    print(SUBFOLDER)
    os.makedirs(directory + "/results/" + MODEL + SUBFOLDER)

    PREDICT = len(sys.argv) > 3
    if MODEL == "Google":
        embeddings_size = 300           #  dimensionality of word embeddings
    elif MODEL == "Stanford":
        embeddings_size = 200           #  dimensionality of word embeddings
    elif MODEL == "Own":
        embeddings_size = 250
    else:
        raise Exception("No such model!")

    # Loading generated word embeddings
    print("Loading embeddings")
    if MODEL == "Own":
        own_model = word2vec.Word2Vec.load(directory +'/Embeddings/ownEmbeddings250')
    elif MODEL == "Google":
        google_model = word2vec.Word2Vec.load_word2vec_format(directory +'/Embeddings/Google-vectors-negative300.bin', binary=True)
    else:
        stanford_dict_model = {}
        with open(directory +"/Embeddings/glove.twitter.27B.200d.txt", encoding="UTF8") as f:
            for line in f:
                splittedLine = line.split()
                word = splittedLine[0];
                word_vector = splittedLine[1:]
                stanford_dict_model[word] = word_vector
    np.random.seed(0)
    print ("Embeddings loaded")


    #################### Create the CNN #####################
    print ("Create the CNN")
    max_sentence_length = 30        #  maximal sentence length
    
    # Defining CNN architecture
    n_filters_1 = 100       	# number of filters in the first convolutional layer
    filter_size_1 = 10       	# filter size in the first convolutional layer
    n_neurons_1 = 512       	# number of neurons in the first fully-connected layer
    n_neurons_2 = 256       	# number of neurons in the second fully-connected layer
    keep_probability_1 = 0.5
    keep_probability_2 = 0.5
    pooling = "max" #"avg"

    # 1 - Convolutional and Pooling layers:
    W_conv1 = weight_variable([embeddings_size, filter_size_1, 1, n_filters_1], stddev=0.01, pName="w_conv1")
    b_conv1 = bias_variable([n_filters_1], pName="b_conv1")
    x = tf.placeholder(tf.float32, [None, max_sentence_length * embeddings_size])   # Input into the network
    x_image = tf.reshape(x, [-1, embeddings_size, max_sentence_length, 1])          # x is the tensor ([batch, height, width, channels], [shape with -1 stays constant]
    h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
    if pooling == "avg":
	    h_pool1 = avg_pool_layer(h_conv1)
    else:
	    h_pool1 = max_pool_layer(h_conv1)


    # 2 - Fully connected layers with Dropout:
    # first
    W_fc1 = weight_variable([n_filters_1, n_neurons_1], stddev=0.01, pName="w_fc1")
    b_fc1 = bias_variable([n_neurons_1], pName="b_fc1")
    h_pool1_flat = tf.reshape(h_pool1, [-1, n_filters_1])                   # Hier wird so umgebaut, dass 
    h_fc1 = tf.nn.relu(tf.matmul(h_pool1_flat, W_fc1) + b_fc1)
    keep_prob_1 = tf.placeholder(tf.float32)                                # Keep Probability is input value
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob_1)                          # Remove some values to work agains overfitting...

    # second
    W_fc2 = weight_variable([n_neurons_1, n_neurons_2], stddev=0.01, pName="w_fc2")
    b_fc2 = bias_variable([n_neurons_2], pName="b_fc2")
    h_fc2 = tf.nn.relu(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)
    keep_prob_2 = tf.placeholder(tf.float32)
    h_fc2_drop = tf.nn.dropout(h_fc2, keep_prob_2)


    # 3 - Softmax layer
    W_softmax = weight_variable([n_neurons_2, 2], stddev=0.01, pName="W_softmax")              # The output is a value [1,0] or [0,1]
    b_softmax = bias_variable([2], pName="b_softmax")          
    y_conv = tf.nn.softmax(tf.matmul(h_fc2_drop, W_softmax) + b_softmax)    # The result of the CNN
    y_ = tf.placeholder(tf.float32, [None, 2])                              # Correct Testvalue, also [1,0] or [0,1]

    # Cross entropy loss function and L2 regularization term
    cross_entropy = -tf.reduce_sum(y_ * tf.log(tf.clip_by_value(y_conv, 1e-10, 1.0)))
    cross_entropy += 5e-2 * (tf.nn.l2_loss(W_fc1) + tf.nn.l2_loss(b_fc1) +
                             tf.nn.l2_loss(W_fc2) + tf.nn.l2_loss(b_fc2))
    tf.scalar_summary("CrossEntropy", cross_entropy)

    # 4 - Training step
    train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)


    # 5 - Computing accuracy
    correct_predictions = tf.equal(tf.argmax(y_conv,1), tf.argmax(y_,1))
    accuracy = tf.reduce_mean(tf.cast(correct_predictions, tf.float32))

    # 6 - Writing summaries
    tf.scalar_summary("Accuracy", accuracy)  
    summary_op = tf.merge_all_summaries() 

    if not PREDICT:
        # Set the parametrisation
        print("Training " + MODEL)        
        NUM_TRAINING_ITERATIONS = 40000
        ITERATIONS_PER_LOG = 100
        

        #################### Data Generation #####################
        # Generating training data
        print("Generating data... ")
        if MODEL == "Stanford" or MODEL == "Own":
            with open(directory +"/data/train_pos_full_preprocessed.txt",encoding='UTF-8') as f:
                content1 = f.readlines()    
            with open(directory +"/data/train_neg_full_preprocessed.txt", encoding='UTF-8') as f:
                content2 = f.readlines()
        else:
            with open(directory +"/data/train_pos_full.txt",encoding='UTF-8') as f:
                content1 = f.readlines()    
            with open(directory +"/data/train_neg_full.txt", encoding='UTF-8') as f:
                content2 = f.readlines()
        content = content1 + content2
        train_size = len(content)
        train_answers = np.zeros((train_size, 2))
        num_positives = len(content1)
        train_answers[:num_positives] = [1, 0]
        train_answers[num_positives:] = [0, 1]

        # Generating test data
        idx_test = np.random.randint(0, train_size, 200)
        data_test = np.zeros((0,  max_sentence_length * embeddings_size))
        labels_test = train_answers[idx_test]
        labels_test_unary = np.argmax(labels_test, axis=1)
        for i in idx_test:    
            data_test = np.vstack((data_test, sentence_to_vector(content[i])))
        content = np.delete(content, idx_test, axis = 0)
        train_answers = np.delete(train_answers, idx_test, axis = 0)
        train_size = len(content)



        #################### Execute the CNN #####################
        # write infos into the folder
        with open(directory + "/results/" + MODEL + SUBFOLDER + '/description.txt', 'w+') as f:
        	f.write('{0}: {1}\n'.format("Embeddings Size", str(embeddings_size)))
        	f.write('{0}: {1}\n'.format("Model", MODEL))
        	f.write('{0}: {1}\n'.format("Training Iterations", str(NUM_TRAINING_ITERATIONS)))
        	f.write("\n")
        	f.write('{0}: {1}\n'.format("n_filters_1", str(n_filters_1)))
        	f.write('{0}: {1}\n'.format("filter_size_1", str(filter_size_1)))
        	f.write('{0}: {1}\n'.format("n_neurons_1", str(n_neurons_1)))
        	f.write('{0}: {1}\n'.format("n_neurons_2", str(n_neurons_2)))
        	f.write('{0}: {1}\n'.format("MaxSentenceLenght", str(max_sentence_length)))
        	f.write('{0}: {1}\n'.format("keep_probability_1", str(keep_probability_1)))
        	f.write('{0}: {1}\n'.format("keep_probability_2", str(keep_probability_2)))
        	f.write('{0}: {1}\n'.format("Pooling", str(pooling)))


        start = time.time()
        # Running Tensorflow session
        sess = tf.Session()
        summary_writer = tf.train.SummaryWriter(directory + "/results/" + MODEL + SUBFOLDER , graph=sess.graph)
        sess.run(tf.initialize_all_variables())

        # Load the pretrained session. (WORKS!)
        #saver = tf.train.Saver()
        #save_path = saver.restore(sess, directory + "/results/" + MODEL + "/model.ckpt")
        #print("Model restored.")

        # Training CNN
        print("\n\nTraining CNN... ")
        batch_size = 100
        accuracy_train = 0.0
        for i in range(NUM_TRAINING_ITERATIONS):
            if i == int(NUM_TRAINING_ITERATIONS / 4):
                saver = tf.train.Saver()
                save_path = saver.save(sess, directory + "/results/" + MODEL + SUBFOLDER + "/model25.ckpt")
                print("Model saved in file: %s" % save_path)
            if i == int(NUM_TRAINING_ITERATIONS / 2):
                saver = tf.train.Saver()
                save_path = saver.save(sess, directory + "/results/" + MODEL + SUBFOLDER + "/model50.ckpt")
            if i == int(NUM_TRAINING_ITERATIONS / 4 * 3):
                saver = tf.train.Saver()
                save_path = saver.save(sess, directory + "/results/" + MODEL + SUBFOLDER + "/model75.ckpt")

            # Generating a new minibatch for training the model:
            idx_train = np.random.randint(0, train_size, batch_size)          
            train_answ = train_answers[idx_train]
            
            sentences = content[idx_train]    
            train_data = np.zeros((0, max_sentence_length * embeddings_size))
            
            for s in sentences:
                train_data = np.vstack((train_data, sentence_to_vector(s)))

            train_data = np.reshape(train_data, [batch_size, max_sentence_length * embeddings_size])
                 
            # Performing training step (set the placeholders to the input data)
            accuracy_temp, temp = sess.run([accuracy, train_step], feed_dict={x: train_data, y_: train_answ, keep_prob_1: keep_probability_1,
                                                                              keep_prob_2: keep_probability_2})
            accuracy_train = accuracy_train + accuracy_temp

            # Performing validation of the model on the test data
            if i % ITERATIONS_PER_LOG  == 0:
                feed_dict={ x : data_test, y_: labels_test, keep_prob_1: 1.0, keep_prob_2: 1.0};
                train_accuracy, train_entropy, y_pred, summary_str = sess.run([accuracy, cross_entropy, y_conv, summary_op], feed_dict=feed_dict)
                print("step %d, accuracy test: %g, accuracy train: %g, time: %g"%(i, train_accuracy, accuracy_train / ITERATIONS_PER_LOG, time.time() - start))
                #print(classification_report(labels_test_unary, np.argmax(y_pred, axis=1), digits=4))
                accuracy_train = 0.0
                summary_writer.add_summary(summary_str, i)


        # Output and save variables on disk.
        print("Timeconsumption for training: " + str(time.time()-start))
        print("Overall amount of unknown words: " + str(OverallamountOfUnknownWords))
        saver = tf.train.Saver()
        save_path = saver.save(sess, directory + "/results/" + MODEL + SUBFOLDER + "/model.ckpt")
        print("Model saved in file: %s" % save_path)




    # Generate single predictions for Kaggle test set
    else:
        print ("Predicting " + MODEL)
        # Load the trained session.
        sess = tf.Session()
        saver = tf.train.Saver()
        save_path = saver.restore(sess, directory + "/results/" + MODEL + SUBFOLDER + "/model.ckpt")
        print("Model restored.")

        # Load the data to predict        
        predict_array = np.zeros((10000, max_sentence_length * embeddings_size))  
        i = 0   
        if MODEL == "Stanford" or MODEL == "Own":
             file = open(directory + '/data/test_data_preprocessed.txt')
        else:
            file = open(directory + '/data/test_data.txt')         
        for line in file:
            predict_array[i] = sentence_to_vector(line[line.find(',')+1:])
            i += 1
            if i >= 10000:
                break;
    
        y_pred = np.zeros((0, 2))
        for i in range (0, 10000, 200):
            y_pred_temp = sess.run(y_conv, feed_dict={ x : predict_array[i:(i + 200)], y_: np.zeros((10000, 2)), keep_prob_1: 1.0,
                                                                          keep_prob_2: 1.0})
            y_pred = np.vstack((y_pred, y_pred_temp))
        
        y_pred_unary = -1*(np.argmax(y_pred, axis=1)*2 - 1)
        predictions = np.zeros((10000, 2))
        for i in range(10000):
            predictions[i][0] = i + 1
            predictions[i][1] = y_pred_unary[i]
        
        np.savetxt(directory + '/results/' + MODEL + SUBFOLDER + '/submission.csv', predictions, fmt='%d', delimiter = ',', header = 'Id,Prediction') 
        

