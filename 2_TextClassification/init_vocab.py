# Generating word embeddings from Tiwtter data
# using word2vec model (implemented in gensim python package)

from gensim.models import word2vec
import logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
sentences = word2vec.LineSentence('data/all_preprocessed.txt')

model = word2vec.Word2Vec(size=250, window=5, min_count=5, workers=48)
model.build_vocab(sentences)

for i in range(1):
    model.train(sentences)    
    # Debug tests
    print(model.most_similar('nigga'))
    
model.save('embeddings250')
