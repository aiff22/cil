# Builds the committee out of the 3 different neural networks
import itertools 
import os
import numpy as np

if __name__ == "__main__":
    directory = os.path.dirname(os.path.realpath(__file__))
    google_predict = open(directory + '/results/Google/submission.csv') 
    own_predict = open(directory + '/results/Own/submission.csv')
    own_predict2 = open(directory + '/results/Own/100d/submission.csv') 
    own_predict3 = open(directory + '/results/Own/300d/submission.csv')  
    #own_predict4 = open(directory + '/results/Own/600d/submission.csv')  
    stanford_predict = open(directory + '/results/Stanford/10kSteps/submission.csv') 

    final_predict = open(directory + '/results/Committee/submission.csv', "w+")
    final_predict.write("Id,Prediction\n")
    firstLine = True
    for x, y, z, p, q in zip(google_predict, own_predict,own_predict2,own_predict3, stanford_predict):
        if firstLine:
            firstLine = False
            continue

        l = x.split(",")[0]
        p1 = int(x.split(",")[1][:-1])
        p2 = int(y.split(",")[1][:-1])
        p3 = int(z.split(",")[1][:-1])
        #p4 = int(p.split(",")[1][:-1])
        p5 = int(q.split(",")[1][:-1])
        final_predict.write(str(l) + "," + str(np.sign(p1 + p2 + p3 + p5)) + "\n")

    final_predict.close()