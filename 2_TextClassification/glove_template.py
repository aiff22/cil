#Template provided by CIL Team
#!/usr/bin/env python3
from scipy.sparse import *
import numpy as np
import pickle
import random


def main():
    print("loading cooccurrence matrix")
    with open('./2_TextClassification/data/cooc.pkl', 'rb') as f:
        cooc = pickle.load(f)
    print("{} nonzero entries".format(cooc.nnz))

    nmax = 100
    print("using nmax =", nmax, ", cooc.max() =", cooc.max())

    print("initializing embeddings")
    embedding_dim = 20
    X = np.random.normal(size=(cooc.shape[0], embedding_dim)) # X
    Y = np.random.normal(size=(cooc.shape[1], embedding_dim)) # Y

    eta = 0.001
    alpha = 3 / 4

    epochs = 10

    for epoch in range(epochs):
        print("epoch {}".format(epoch))
        # This is an iteration over the sparse entries
        for ix, jy, n in zip(cooc.row, cooc.col, cooc.data):
            # First determine target m
            m = np.log(n)

            # Calc the weighting function
            fn = min(1,(n/nmax)**alpha)
            
            # Do the SGD step
            scaling =  2 * eta * fn * (m - np.dot(X[ix,:],Y[jy,:]))
            X[ix,:] += scaling * Y[jy,:]
            Y[jy,:] += scaling * X[ix,:] 

    np.save('embeddings', xs)


if __name__ == '__main__':
    main()
