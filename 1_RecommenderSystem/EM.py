import numpy as np
import scipy.sparse.linalg as sp
import random
import os

def process_data(path, m, n):

    data = []
    data_train = []
    data_test = []
    
    with open(path, 'r') as file:
        header = file.readline()
        for line in file:
            info, rating = line.split(",")
            rating = int(rating)
            row, col = info.split("_")
            row = int(row[1:])
            col = int(col[1:])
            
            data.append((row - 1, col - 1, rating))

    random.shuffle(data)
    train_size = int(len(data)*0.9)
    
    data_train = data[:train_size]
    data_test = data[train_size:]

    ratings_all = np.zeros((m, n))
    ratings_train = np.zeros((m, n))
    ratings_test = np.zeros((m, n))

    for (i, j, r) in data:
        ratings_all[i, j] = r

    for (i, j, r) in data_train:
        ratings_train[i, j] = r

    for (i, j, r) in data_test:
        ratings_test[i, j] = r
    
    return ratings_all, ratings_train, ratings_test

def predict_average(ratings):

    nonzero = (ratings != 0)

    # average user rating
    rating_num = nonzero.sum(1)
    rating_sum = ratings.sum(1)
    user_ratings = rating_sum / rating_num
    user_ratings[np.isnan(user_ratings)] = 3
    
    # average movie rating
    rating_num = nonzero.sum(0)
    rating_sum = ratings.sum(0)
    movie_ratings = rating_sum / rating_num
    
    predictions = ratings
    alpha = 0.4

    for col in range(ratings.shape[1]):
        no_rating = (predictions[:, col] == 0)
        predictions[no_rating, col] = -1 * alpha * movie_ratings[col]

    for row in range(ratings.shape[0]):
        no_rating = (predictions[row, :] <= 0)
        predictions[row, no_rating] = -1 * predictions[row, no_rating] + (1 - alpha)* user_ratings[row]
            
    return predictions

def predict_svd(ratings, k = 12):

    avg_prediction = predict_average(ratings)
    U, d, V = sp.svds(avg_prediction, k = k)
    
    D = np.diag(d)
    prediction = np.dot(U, np.dot(D, V))
    
    return prediction

def get_score(predicted_ratings, test_ratings):
    
    to_check = (test_ratings != 0)

    predected = predicted_ratings[to_check]
    real = test_ratings[to_check]
    
    error = np.sum(np.square(np.abs(predected - real)))    
    score = np.sqrt(error / np.sum(to_check))
    
    return score

def write_to_file(ratings):

    data = []
    path = 'sampleSubmission.csv'
    
    with open(path, 'r') as file:
        header = file.readline()
        for line in file:
            info, rating = line.split(",")
            rating = int(rating)
            row, col = info.split("_")
            row = int(row[1:])
            col = int(col[1:])
            
            data.append((row - 1, col - 1, rating))

    to_print = "Id,Prediction\n"
    
    for (i, j, r) in data:
        to_print += "r%d_c%d,%f\n" % (i + 1, j + 1, ratings[i, j])

    path_write = 'submission.csv'
    with open(path_write, 'w') as output_file:
        output_file.write(to_print)
    


ratings_all, ratings_train, ratings_test = process_data('data_train.csv', 10000, 1000)

prediction = predict_svd(ratings_train)
score = get_score(prediction, ratings_test)

print("Prediction score: %.4f" % score)

submission = predict_svd(ratings_all)
write_to_file(submission)