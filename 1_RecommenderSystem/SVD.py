import numpy as np
import scipy.sparse.linalg as sp
import random
import os
import matplotlib.pyplot as plt
import time 

def process_data(path, m, n):
    data = []
    data_train = []
    data_test = []
        
    # Parse file into data array
    with open(path, 'r') as file:
        header = file.readline()
        for line in file:
            info, rating = line.split(",")
            rating = int(rating)
            row, col = info.split("_")
            row = int(row[1:])
            col = int(col[1:])

            data.append((row - 1, col - 1, rating))

    # Split available ratings into whole, training and test set
    length = len(data)
    test_size = int(length*0.1)
    random.seed(1);
    idx_testdata = set(random.sample(range(length),test_size))
    ratings_all = np.zeros((m, n))
    ratings_train = np.zeros((m, n))
    ratings_test = np.zeros((m, n))
    for counter, (i, j, r) in enumerate(data):
        ratings_all[i, j] = r
        if (counter in idx_testdata):
            ratings_test[i, j] = r
        else:
            ratings_train[i, j] = r

    return ratings_all, ratings_train, ratings_test



def predict_average(ratings):
    nonzeros = (ratings != 0)

    # average user rating (add all elements per line)
    rating_num = nonzeros.sum(1)
    rating_sum = ratings.sum(1)
    user_avg_ratings = rating_sum / rating_num
    user_avg_ratings[np.isnan(user_avg_ratings)] = 3
    
    # average movie rating (add all elements per column)
    rating_num = nonzeros.sum(0)
    rating_sum = ratings.sum(0)
    movie_avg_ratings = rating_sum / rating_num
    
    # Merge average movie ratings and user ratings (clever trick with -1)
    predictions = np.copy(ratings)
    alpha = 0.4
    for col in range(ratings.shape[1]):
        no_rating = (predictions[:, col] == 0)
        predictions[no_rating, col] = -1 * alpha * movie_avg_ratings[col]
    for row in range(ratings.shape[0]):
        no_rating = (predictions[row, :] <= 0)
        predictions[row, no_rating] = -1 * predictions[row, no_rating] + (1 - alpha)* user_avg_ratings[row]
            
    return predictions



def predict_svd(given_ratings, k = 12, roundingthreshold = 0, clipping = True, pretrainedMatrix = None):
    # Calculate missing ratings
    if (pretrainedMatrix is None):
        avg_prediction = predict_average(given_ratings)
    else:
        avg_prediction = pretrainedMatrix

    #u, s, v= np.linalg.svd(avg_prediction)
    #plt.plot(s)
    #plt.show()

    U, d, V = sp.svds(avg_prediction, k = k)
    D = np.diag(d)
    
    # Enhance given ratings by predicted ones at empty positions (keep the already given ones correct)
    prediction = given_ratings + np.dot(U, np.dot(D, V)) * (given_ratings == 0)
    
    # Do rounding if rounding threshhold is given (set threshold to 0.5 if you want to round all values) 
    if (roundingthreshold > 0):
        prediction = prediction - (prediction - np.round(prediction)) * (np.abs(np.round(prediction) - prediction) < 0.2)

    # Do clipping if requested
    if (clipping):
        prediction = np.clip(prediction,1,5)

    return prediction



def get_score(predicted_ratings, test_ratings):
    to_check = (test_ratings != 0)

    predicted = predicted_ratings[to_check]
    real = test_ratings[to_check]
    
    error = np.sum(np.square(np.abs(predicted - real)))    
    score = np.sqrt(error / np.sum(to_check))
    
    return score



def write_to_file(ratings):
    # get sample submission
    data = []
    path = './1_RecommenderSystem/data/sampleSubmission.csv'
    with open(path, 'r') as file:
        header = file.readline()
        for line in file:
            info, rating = line.split(",")
            rating = int(rating)
            row, col = info.split("_")
            row = int(row[1:])
            col = int(col[1:])
            
            data.append((row - 1, col - 1, rating))

    # write own results  for elements
    path_write = './1_RecommenderSystem/results/submission.csv'
    with open(path_write, 'w') as output_file:
        output_file.write("Id,Prediction\n")
        for (i, j, r) in data:
            output_file.write("r"+ str(i + 1) +"_c" + str(j + 1) + "," + str(ratings[i, j]) + "\n")
    




# MAIN -------------------
if __name__ == "__main__":
    ratings_all, ratings_train, ratings_test = process_data('./1_RecommenderSystem/data/data_train.csv', 10000, 1000)

    ## First try without clipping
    #prediction = predict_svd(ratings_train, clipping = False)
    #score = get_score(prediction, ratings_test)
    #print("Prediction score without clipping: %.4f" % score)
    
    ## Then try with clipping
    #prediction = predict_svd(ratings_train)
    #score = get_score(prediction, ratings_test)
    #print("Prediction score with clipping: %.4f" % score)
       


    # Do a dimension reduction in multiple steps
    steps = [800, 500, 300, 200, 100, 50, 15]
    prediction = None;
    for k in steps:
        prediction = predict_svd(ratings_all, k, pretrainedMatrix = prediction)
        score = get_score(prediction, ratings_all)
        print("Prediction for " + str(k) + ": " + str(score) + "\n")

    
    prediction = predict_svd(ratings_all, 10, pretrainedMatrix = prediction)
    score = get_score(prediction, ratings_all)
    print("Prediction for " + str(12) + ": " + str(score) + "\n")


    ## Experiment with rounding
    #prediction = predict_svd(ratings_train, roundingthreshold = 0.1)
    #score = get_score(prediction, ratings_test)
    #print("Prediction score slight round: %.4f" % score)

    #prediction = predict_svd(ratings_train, roundingthreshold = 0.3)
    #score = get_score(prediction, ratings_test)
    #print("Prediction score full round: %.4f" % score)

    #submission = predict_svd(ratings_all)
    write_to_file(prediction)

